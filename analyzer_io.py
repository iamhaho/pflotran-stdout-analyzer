#!/bin/env python
"""
A module to read screen output of PFLOTRAN.

@author: Heeho Park <heepark@sandia.gov>
"""

import re
import os

def scanfolder(parent_dir, extension):
    """
    input:
    parent_dir: all the subdirectories under this directory will be searched 
                for a file with [extension]
    extension: a file extension that this function should look for
               e.g. pflotran.stdout -> extension=stdout

    output: 
    file_list: a list of files and their paths that has the [extension]
    experiment_list: the directory name that contains the file with [extension]

    Description:
    scans all subdirectories in search for a file with [extension]
    each experiment must be separated by each directory

    e.g. wipp/ilu0/pflotran.stdout, wipp/ilu1/pflotran.stdout, etc
        -> experiment_list = ['ilu0', 'ilu1', ...]
 
    """
    file_list = []
    experiment_list = []
    # find all files with an extension under parent dir and its sub dirs
    for path, subdirs, files in os.walk(parent_dir):
        for target_file in files:
            # probably not the best way to find file extensions but it works
            # to some extent
            if target_file.endswith(extension):
                filename = os.path.join(path,target_file)
                file_list.extend([filename])
                experiment_list.extend([filename.split('/')[-2]])
    #return file_path strings in lists
    
    return file_list,experiment_list


def scan_screen_output(file_list):
    """
    input:
    file_list: provide a list of absolute paths of files as an input

    output:
    max_failed_time_step: out of all the runs, this function finds the maximum
                          time step that is available in all runs to compare
    simulation_final_time: retrieves the final simulation time from a 
                           simulation that completed

    Description:
    Scans all screen output files to gather information needed for read
    screen output function.
    """

    max_failed_time_step = 1000000000
    time_step = 100000000
    simulation_final_time = 0
    final_time = 0    
    for afile in file_list:
        with open(afile, 'r') as f:
            while True:
                line = f.readline()
                if not line:
                    break
                if ' Step ' in line:
                    words = line.split()
                    time_step = int(words[1])
                    final_time = float(words[3])
        max_failed_time_step = min(max_failed_time_step, time_step)
        simulation_final_time = max(simulation_final_time, final_time)
    return max_failed_time_step, simulation_final_time

    
def read_screen_output(file_list, fixed_time_step = 0,
                       simulation_final_time = 0):
    """
    input: 
    file_list:  a provide list of absolute paths of files as an input 
    fixed_time_step: print results at a fixed time step
    simulation_final_time: simulation time for all tests

    output:
    tmp_dict: dictionary that includes all performance information

    Description:
    reads all screen output to compile performance information for 
    each test case trials.    """
    
    tmp_dict = {}
    for afile in file_list:
        print (afile)
        exp = afile.split('/')[-2]
        tmp_dict[exp] = {'Fixed_TS':0,
                         'Fixed_LI':0,
                         'Fixed_NI':0,
                         'Fixed_LI_per_NI':0.0,
                         'Fixed_NI_per_TS':0.0,
                         'Fixed_Cuts':0,
                         'Fixed_Cuts_per_TS':0.0,
                         'Fixed_Sim_Time':0.0,
                         'Fixed_Dt':0.0,
                         'Fixed_Time_Unit':'',
                         'Final_TS':0,
                         'Final_LI':0,
                         'Final_NI':0,
                         'Final_LI_per_NI':0.0,
                         'Final_NI_per_TS':0.0,
                         'Final_Cuts':0,
                         'Final_Cuts_per_TS':0.0,
                         'Final_Sim_Time':0.0,
                         'Final_Sim_Time_Unit':'',
                         'Final_SNES_Time':0.0,
                         'Final_SNES_Time_Unit':'',
                         'Final_Wall_Time':0.0,
                         'Final_Wall_Time_Unit':'',
                         'Final_wasted_LI':-1,
                         'Final_wasted_NI':-1,
                         'Timestepper_accel':'',
                         'Timestepper_thres':0,
                         'Timestepper_ramp':[],
                         'Timestepper_rf':0,
                         'Timestepper_mgf':0,
                         'NS_atol':0.,
                         'NS_rtol':0.,
                         'NS_stol':0.,
                         'NS_max_iter':0,
                         'NS_mat_type':'',
                         'NS_pc_mat_type':'',
                         'LS_solver':'',
                         'LS_precond':'',
                         'LS_atol':0.,
                         'LS_rtol':0.,
                         'LS_maxit':0.,
        }

        with open(afile, 'r') as f:
            tmp = tmp_dict[exp]
            while True:
                line = f.readline()
                if not line:
                    break
                
                if 'FLOW Time Stepper' in line:
                    while True:
                        line = f.readline().strip()
                        word = line.split(':')[-1].strip()
                        if 'acceleration:' in line:
                            tmp['Timestepper_accel'] = word
                        elif 'acceleration threshold:' in line:
                            tmp['Timestepper_thres'] = int(word)
                        elif 'ramp entry' in line:
                            tmp['Timestepper_ramp'].append(float(word))
                        elif 'reduction factor:' in line:
                            tmp['Timestepper_rf'] = float(word)
                        elif 'maximum growth factor:' in line:
                            tmp['Timestepper_mgf'] = float(word)
                            break
                        elif line == '':
                            break
                        
                if 'FLOW Newton Solver' in line:
                    while True:
                        line = f.readline().strip()
                        word = line.split(':')[-1].strip()
                        if 'atol:' in line:
                            tmp['NS_atol'] = float(word)
                        elif 'rtol:' in line:
                            tmp['NS_rtol'] = float(word)
                        elif 'stol:' in line:
                            tmp['NS_stol'] = float(word)
                        elif 'max iter:' in line:
                            tmp['NS_max_iter'] = int(word)
                        elif 'matrix type:' in line:
                            if 'precond.' in line:
                                tmp['NS_pc_mat_type'] = word
                            else: 
                                tmp['NS_mat_type'] = word
                        elif 'check infinity norm:' in line:
                            break
                        
                if 'FLOW Linear Solver' in line:
                    while True:
                        line = f.readline().strip()
                        word = line.split(':')[-1].strip()
                        if 'solver:' in line:
                            tmp['LS_solver'] = word
                        elif 'preconditioner:' in line:
                            tmp['LS_precond'] = word
                        elif 'atol:' in line:
                            tmp['LS_atol'] = float(word)
                        elif 'rtol:' in line:
                            tmp['LS_rtol'] = float(word)
                        elif 'maximum iteration:' in line:
                            tmp['LS_maxit'] = int(word)
                            break
                        
                if ' Step ' in line and not 'FLOW TS BE' in line:
                    words = line.split()
                    if int(words[1]) == fixed_time_step:
                        tmp['Fixed_TS'] = int(words[1])
                        tmp['Fixed_Sim_Time'] = float(words[3])
                        tmp['Fixed_Dt'] = float(words[5])

                        tmp['Fixed_Time_Unit'] = words[6]
                        line2 = f.readline()
                        # find numbers between brackets using regex
                        nums = re.findall(r"\[\s*\+?(-?\d+)\s*\]", line2) 
                        tmp['Fixed_NI'] = int(nums[0])
                        tmp['Fixed_LI'] = int(nums[1])
                        tmp['Fixed_Cuts'] = int(nums[2])
                        li_ni = tmp['Fixed_LI']/float(tmp['Fixed_NI'])
                        ni_ts = tmp['Fixed_NI']/float(tmp['Fixed_TS'])
                        cuts_ts = tmp['Fixed_Cuts']/float(tmp['Fixed_TS'])
                        tmp['Fixed_Cuts_per_TS'] = cuts_ts
                        tmp['Fixed_LI_per_NI'] = li_ni
                        tmp['Fixed_NI_per_TS'] = ni_ts
                    if abs(float(words[3]) - simulation_final_time) < 1.e-10:
                        tmp['Final_Sim_Time'] = float(words[3])
                        tmp['Final_Sim_Time_Unit'] = words[6]
                if 'FLOW TS BE steps' in line:
                    # find numbers between spaces using regex
                    nums = re.findall(r"\s*\+?(-?\d+)\s*", line)
                    tmp['Final_TS'] = int(nums[0])
                    tmp['Final_NI'] = int(nums[1])
                    tmp['Final_LI'] = int(nums[2])
                    tmp['Final_Cuts'] = int(nums[3])
                    words4 = f.readline().split()  # wasted linear iterations
                    tmp['Final_wasted_LI'] = int(words4[-1])
                    words5 = f.readline().split()  # wasted nonlinear iterations
                    tmp['Final_wasted_NI'] = int(words5[-1])
                    words2 = f.readline().split()
                    tmp['Final_SNES_Time'] = float(words2[-2])
                    tmp['Final_SNES_Time_Unit'] = words2[-1]
                if 'Wall Clock Time' in line:
                    words3 = line.split()
                    tmp['Final_Wall_Time'] = float(words3[-4])
                    tmp['Final_Wall_Time_Unit'] = words3[-3]
                    li_ni = tmp['Final_LI']/float(tmp['Final_NI'])
                    ni_ts = tmp['Final_NI']/float(tmp['Final_TS'])
                    cuts_ts = tmp['Final_Cuts']/float(tmp['Final_TS'])
                    tmp['Final_Cuts_per_TS'] = cuts_ts    
                    tmp['Final_LI_per_NI'] = li_ni
                    tmp['Final_NI_per_TS'] = ni_ts
                if 'Simulation failed.' in line:
                    break
    return tmp_dict  # tmp carries information of each simulation

def read_cut_reasons(file_list):
    """
    input:
    file_list: a provide list of absolute paths of files as an input

    output:
    tmp: a dictionary keyed with experiment name with failure counts 
         for each reason 

    Description:
    Reads stdout files and counts each failed reasons.
    NEWTON ITERATIONS
    snes=-1: the new x location passed the function is not 
             in the domain of F
    snes=-2: Diverged_Function_Count
    snes=-3: the linear solve failed
    snes=-4: The norm of the residual is NaN.
    snes=-5: Tha maximum number of Newton iteration was reached
    snes=-6: the line search failed
    snes=-7: inner solve failed
    snes=-8: ||J^T b|| is small, implies converged to local 
             minimum of F()
    snes=-9: ||F|| > divtol*||F_initial||
    others : declared diverged due to reasons from PFLOTRAN
    LINEAR ITERATIONS
    maxits   : LS took too many iterations and reached maxits
    diverged : LS diverged due to dtol based on the 
               norm(r) >= dtol*norm(b) where r = b-Ax
    breakdown: A breakdown in the Krylov method was detected so the
               method could not continue to enlarge the Krylov space.
    """
    tmp = {}
    for afile in file_list:
        exp_name = afile.split('/')[-2]
        # snes failed reasons:                        linear failed reasons:
        #                -1 -2 -3 -4 -5 -6 -7 -8 -9   maxits diverged breakdown
        # index           0  1  2  3  4  5  6  7  8  9  10      11       12
        tmp[exp_name] = {'snes=-1' : 0, 'snes=-2' : 0, 'snes=-3' : 0,
                         'snes=-4' : 0, 'snes=-5' : 0, 'snes=-6' : 0,
                         'snes=-7' : 0, 'snes=-8' : 0, 'snes=-9' : 0,
                         'snes=-11': 0, 'snes=-88': 0,
                         'snes=other' : 0, 'snestotal' : 0,
                         'snesfrac=-1' : 0.0, 'snesfrac=-2' : 0.0, 
                         'snesfrac=-3' : 0.0, 'snesfrac=-4' : 0.0, 
                         'snesfrac=-5' : 0.0, 'snesfrac=-6' : 0.0,
                         'snesfrac=-7' : 0.0, 'snesfrac=-8' : 0.0, 
                         'snesfrac=-9' : 0.0, 'snesfrac=other' : 0.0, 
                         'snesfrac=-11': 0.0,
                         'lstotal' : 0,
                         'ls=maxit' : 0, 'ls=divgd' : 0,
                         'ls=brkdn' : 0, 'lsfrac=maxit' : 0.0, 
                         'lsfrac=divgd' : 0.0, 'lsfrac=brkdn' : 0.0}
        tmp_dict = tmp[exp_name]
        with open(afile, 'r') as f:
            while True:
                line = f.readline()
                if not line:
                    break
                if 'Cut time step:' in line:
                    start = line.find('snes=')+5
                    end = start+3
                    reason = int(line[start:end])
                    if 'snes= -1' in line:
                        tmp_dict['snes=-1'] += 1
                    elif 'snes= -2' in line:
                        tmp_dict['snes=-2'] += 1
                    elif 'snes= -3' in line:
                        tmp_dict['snes=-3'] += 1    
                    elif 'snes= -4' in line:
                        tmp_dict['snes=-4'] += 1    
                    elif 'snes= -5' in line:
                        tmp_dict['snes=-5'] += 1    
                    elif 'snes= -6' in line:
                        tmp_dict['snes=-6'] += 1    
                    elif 'snes= -7' in line:
                        tmp_dict['snes=-7'] += 1    
                    elif 'snes= -8' in line:
                        tmp_dict['snes=-8'] += 1    
                    elif 'snes= -9' in line:
                        tmp_dict['snes=-9'] += 1
                    elif 'snes=-11' in line:
                        tmp_dict['snes=-11'] += 1    
                    elif 'snes=-88' in line:
                        tmp_dict['snes=-88'] += 1    
                    else:
                        tmp_dict['snes=other'] += 1    
                    
                    line = f.readline()
                    line = f.readline()
                    if 'maxits' in line:
                        tmp_dict['ls=maxit'] += 1
                    elif 'diverged' in line:
                        tmp_dict['ls=divgd'] += 1
                    elif 'breakdown' in line:
                        tmp_dict['ls=brkdn'] += 1
            tmp_dict['snestotal'] = tmp_dict['snes=-1']+tmp_dict['snes=-2']+\
                                     tmp_dict['snes=-3']+tmp_dict['snes=-4']+\
                                     tmp_dict['snes=-5']+tmp_dict['snes=-6']+\
                                     tmp_dict['snes=-7']+tmp_dict['snes=-8']+\
                                     tmp_dict['snes=-11']+tmp_dict['snes=-88']+\
                                     tmp_dict['snes=-9']+tmp_dict['snes=other']
            tmp_dict['lstotal'] = tmp_dict['ls=maxit']+tmp_dict['ls=divgd']+\
                                   tmp_dict['ls=brkdn']
            if tmp_dict['snestotal'] == 0:
                tmp_dict['snestotal'] = 1.0
            if tmp_dict['lstotal'] == 0:
                tmp_dict['lstotal'] = 1.0  
            tmp_dict['snesfrac=-1'] = float(tmp_dict['snes=-1'])/ \
                                      tmp_dict['snestotal']
            tmp_dict['snesfrac=-2'] = float(tmp_dict['snes=-2'])/ \
                                      tmp_dict['snestotal']
            tmp_dict['snesfrac=-3'] = float(tmp_dict['snes=-3'])/ \
                                      tmp_dict['snestotal']
            tmp_dict['snesfrac=-4'] = float(tmp_dict['snes=-4'])/ \
                                      tmp_dict['snestotal']
            tmp_dict['snesfrac=-5'] = float(tmp_dict['snes=-5'])/ \
                                      tmp_dict['snestotal']
            tmp_dict['snesfrac=-6'] = float(tmp_dict['snes=-6'])/ \
                                      tmp_dict['snestotal']
            tmp_dict['snesfrac=-7'] = float(tmp_dict['snes=-7'])/ \
                                      tmp_dict['snestotal']
            tmp_dict['snesfrac=-8'] = float(tmp_dict['snes=-8'])/ \
                                      tmp_dict['snestotal']
            tmp_dict['snesfrac=-9'] = float(tmp_dict['snes=-9'])/ \
                                      tmp_dict['snestotal']
            tmp_dict['snesfrac=-11'] = float(tmp_dict['snes=-11'])/ \
                                      tmp_dict['snestotal']
            tmp_dict['snesfrac=-88'] = float(tmp_dict['snes=-88'])/ \
                                      tmp_dict['snestotal']
            tmp_dict['snesfrac=other'] = float(tmp_dict['snes=other'])/ \
                                      tmp_dict['snestotal']
            tmp_dict['lsfrac=maxit'] = float(tmp_dict['ls=maxit'])/ \
                                      tmp_dict['lstotal']
            tmp_dict['lsfrac=divgd'] = float(tmp_dict['ls=divgd'])/ \
                                      tmp_dict['lstotal']
            tmp_dict['lsfrac=brkdn'] = float(tmp_dict['ls=brkdn'])/ \
                                      tmp_dict['lstotal']
                                     
            # remove unnecessary keys and rename keys
            '''
            if tmp_dict['snes=-1'] == 0:
                tmp_dict.pop('snes=-1')
            else:
                tmp_dict['ni_out_of_domain'] = tmp_dict.pop('snes=-1')
            if tmp_dict['snes=-2'] == 0:
                tmp_dict.pop('snes=-2')
            else:
                tmp_dict['ni_function_count'] = tmp_dict.pop('snes=-2')
            if tmp_dict['snes=-3'] == 0:
                tmp_dict.pop('snes=-3')
            else:
                tmp_dict['ni_linear_solve_fail'] = tmp_dict.pop('snes=-3')
            if tmp_dict['snes=-4'] == 0:
                tmp_dict.pop('snes=-4')
            else:
                tmp_dict['ni_fnorm_NaN'] = tmp_dict.pop('snes=-4')
            if tmp_dict['snes=-5'] == 0:
                tmp_dict.pop('snes=-5')
            else:
                tmp_dict['ni_max_iterations'] = tmp_dict.pop('snes=-5')
            if tmp_dict['snes=-6'] == 0:
                tmp_dict.pop('snes=-6')
            else:
                tmp_dict['ni_line_search_failed'] = tmp_dict.pop('snes=-6')
            if tmp_dict['snes=-7'] == 0:
                tmp_dict.pop('snes=-7')
            else:
                tmp_dict['ni_inner_solve_failed'] = tmp_dict.pop('snes=-7')
            if tmp_dict['snes=-8'] == 0:
                tmp_dict.pop('snes=-8')
            else:
                tmp_dict['ni_converged_to_local_min'] = tmp_dict.pop('snes=-8')
            if tmp_dict['snes=-9'] == 0:
                tmp_dict.pop('snes=-9')
            else:
                tmp_dict['ni_exceeded_divtol'] = tmp_dict.pop('snes=-9')
            if tmp_dict['snes=other'] == 0:
                tmp_dict.pop('snes=other')
            else:
                tmp_dict['ni_PFLOTRAN_break'] = tmp_dict.pop('snes=other')
                
            if tmp_dict['ls=brkdn'] == 0:
                tmp_dict.pop('ls=brkdn')
            else:
                tmp_dict['ls_breakdown'] = tmp_dict.pop('ls=brkdn')
            if tmp_dict['ls=divgd'] == 0:
                tmp_dict.pop('ls=divgd')
            else:
                tmp_dict['ls_diverged'] = tmp_dict.pop('ls=divgd')
            if tmp_dict['ls=maxit'] == 0:
                tmp_dict.pop('ls=maxit')
            else:
                tmp_dict['ls_maxiter'] = tmp_dict.pop('ls=maxit')
            '''
                
    return tmp  # tmp carries information of each simulations time cut reasons


    
    
    
