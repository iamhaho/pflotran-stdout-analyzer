#!/bin/env python
"""
A module post-process and plot performance per trials

@author: Heeho Park <heepark@sandia.gov>
"""

import matplotlib.pyplot as plt
from matplotlib import ticker
import numpy as np
import pandas as pd
#import pylatex as pl
        


def ticks_format(value, index, ticks=[]):
    """
    get the value and returns the value as:
       integer: [0,99]
       1 digit float: [0.1, 0.99]
       n*10^m: otherwise
    To have all the number of the same size they are all returned as latex strings
    """
    exp = np.floor(np.log10(value))
    base = value/10**exp
    # display only these ticks wtih base 2 and 5
    if ticks == []:
        ticks = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]
    if exp == 0 or exp == 1:   
        return '${0:d}$'.format(int(value))
    if exp == -1:
        return '${0:.1f}$'.format(value)
    else:
        if base not in ticks:
            return ""
        else:
            return '${0:d}\\times10^{{{1:d}}}$'.format(int(base), int(exp))

def human_format(num, pos):
    magnitude = 0
    while abs(num) >= 1000:
        magnitude += 1
        num /= 1000.0
    # add more suffixes if you need them
    return '%3g%s' % (num, ['', 'K', 'M', 'G', 'T', 'P'][magnitude])


def create_latex_table(adict, experiments, category, table_caption='table',
                       table_label='generated_table',
                       custom_left_col1=[],
                       custom_left_col2=[], custom_top_row=[], 
                       title_align="left", row_align="center"):
    
    if table_label == "":
        f = open("latex_table.txt", "w")
    else:
        f = open(table_label + ".txt", "w")
    num_col = len(category) + 1
    if custom_left_col2 != []:
        num_col = num_col + 1

    f.write("\\begin{table}[h]\n")
    f.write("\\centering\n")
    f.write("\\begin{tabular}{")
    for i in range(num_col):
        if i == 0:
            if title_align == "right":
                f.write("r ")
            elif title_align == "center":
                f.write("c ")
            else:
                f.write("l ")
        if i > 0:
            if row_align == "right":
                f.write("r ")
            elif row_align == "center":
                f.write("c ")
            else:
                f.write("l ")
    f.write("}\n")
    f.write("\\hline\n")

    for row in range(len(experiments)+1):
        if row == 0:
            if custom_top_row != []:
                for i in range(len(custom_top_row)):
                    if i == len(custom_top_row)-1:
                        f.write(str(custom_top_row[i]).replace("_"," "))
                    else:
                        f.write(str(custom_top_row[i]).replace("_"," ") + " & ")
            else:
                if custom_left_col1 != []:
                    f.write(str(custom_left_col1[row]).replace("_"," ") + 
                            " & ")
                    if custom_left_col2 != []:
                        f.write(str(custom_left_col2[row]).replace("_"," ") + 
                                " & ")
                else:
                    f.write("first col & ")
                for i in range(len(category)):
                    top_row = category[i]
                    if i == len(category)-1:
                        f.write(str(top_row).replace("_"," "))
                    else:
                        f.write(str(top_row).replace("_"," ") + " & ")
            
            f.write("\\\\\n")
            f.write("\\hline\n")
        if row > 0:
            row2 = row - 1
            if custom_left_col1 != []:
                f.write(str(custom_left_col1[row]).replace("_"," ") + " & ")
                if custom_left_col2 != []:
                    f.write(str(custom_left_col2[row]).replace("_"," ") + 
                            " & ")
            else:
                f.write(str(experiments[row2]).replace("_"," ") + " & ")
            for i in range(len(category)):
                value = adict[experiments[row2]][category[i]]
                if category[i] == 'Flow_SNES_Time':
                    value = value/60.
                if i == len(category)-1:
                    f.write(str(value).replace("_"," "))
                else:
                    f.write(str(value).replace("_"," ") + " & ")
            f.write("\\\\\n")
    
    f.write("\\hline\n")
    f.write("\\multicolumn{" + str(num_col) + "}{l}{}\n")
    f.write("\\end{tabular}\n")
    f.write("\\caption{" + table_caption + "}\n")
    f.write("\\label{" + table_label + "}\n")
    f.write("\\end{table}\n")
    f.close()
    return


def create_latex_report():
    pass

    
def plot_stacked_bar_graph(adict, legends=None, legend_flag=True,
                           experiments=None, option=None,
                           option_exact=False, save_img_path=None,
                           save_img_name=None, width=0.9,
                           custom_legends=[],custom_xlabel=[],custom_ylabel='',
                           custom_title='',custom_colormap='', annotate=True,
                           custom_annotate=[],rotate=90,timeunit='min'):
    '''
    input:
    adict: a dictionary from io.read_[functions]
    legends: specify which stats to print. e.g. ['ls=maxits', 'ls=brkdn']
             legends are case sensitive
    option: used only if legends is not specified. options are  case insensitive.
    experiments: enter a list of experiment names or it prints all experiments
    option_exact: option to plot has to have exact keyword or a part of keyword
    '''
    ### determine legends you want to plot
    # select one key as a reference

    refkeys = adict[list(adict.keys())[0]].keys()
    keylength = 0
    for i in adict.keys():
        if len(adict[i].keys()) > keylength:
            refkeys = adict[i].keys()
            keylength = len(adict[i].keys())

    if legends==None:
        legends = []
        if option == None:
            # generate legends for all keys available
            for key in refkeys:
                legends.append(key)
        else:
            for key in refkeys:
                if option_exact:
                   if key.lower() in option:
                     legends.append(key)
                else:
                    if option.lower() in key.lower():
                        legends.append(key)
    else:
        # check and see if all requested keys exist
        for key in legends:
            if key in refkeys:
                continue
            else:
                print ("ERROR: the requested key '%s' does not exist in the" \
                      " dictionary provided. legends are Case Sensitve." \
                      " options are case insensitive." % key)
                return
    
    plot_dict = {}
    for legend in legends:
        plot_dict[legend] = []
    
    ### determine keys you want to plot
    if experiments == None:    
        experiments = sorted(adict.keys())
    else:
        # check and see if all requested keys exist
        for key in experiments:
            if key in adict.keys():
                continue
            else:
                print ("ERROR: the requested key '%s' does not exist in the"
                      " dictionary provided" % key)
                return
                
    xticks = []
    ylabel = ''

    for key in experiments:
        xticks.append(key)
        for legend in legends:
            if legend in adict[key]:
                plot_dict[legend].append(adict[key][legend])
            else:
                plot_dict[legend].append(0)
    
    if 'Final_SNES_Time' in legends:
        ylabel = adict[key]['Final_SNES_Time_Unit']
        if timeunit == 'min':
            for i in range(len(plot_dict['Final_SNES_Time'])):
                plot_dict['Final_SNES_Time'][i] = \
                    plot_dict['Final_SNES_Time'][i]/60.
            ylabel = 'Wall-clock Time [min]'
    elif 'Final_Sim_Time' in legends:
        ylabel = adict[key]['Final_Sim_Time_Unit']
    elif 'Final_Wall_Time' in legends:
        ylabel = adict[key]['Final_Wall_Time_Unit']
    elif 'Fixed_Sim_Time' in legends:
        ylabel = adict[key]['Fixed_Sim_Time_Unit']
        
        
    title = legends[0]
    if len(custom_legends) == len(legends):
        legends = custom_legends
    if len(custom_xlabel) == len(experiments):
        experiments = custom_xlabel
    if custom_ylabel != '':
        ylabel = custom_ylabel
    
    print(legends)
    figx = len(experiments)*1.5*(width+0.1)
    df = pd.DataFrame(plot_dict)
    if custom_colormap != '':
        cmap=custom_colormap
    else:
        cmap='Paired'
    ax = df.plot(kind='bar', stacked=True, colormap=cmap, width=width, 
                 figsize=(figx,5))

    i=0
    second_line = ax.patches[0].get_height()*0.08
    if annotate:
        for p in ax.patches:
            ax.annotate('%.1f'%p.get_height(), 
                        (p.get_x()+p.get_width()/2., p.get_height()*0.98),
                        fontsize=16,ha='center',va='center')
            if custom_annotate != []:
                ax.annotate(custom_annotate[i], 
                        (p.get_x()+p.get_width()/2., p.get_height()*0.98-second_line),
                        fontsize=14,ha='center',va='center')
                i+=1
    ax.tick_params(axis='x', labelsize=16)
    ax.tick_params(axis='y', labelsize=16)
    ax.set_xticklabels(experiments, rotation=rotate,ha="center")
    ax.set_ylabel(ylabel, fontsize=16)
    ax.get_yaxis().set_major_formatter(ticker.FuncFormatter(human_format))
    if len(legends) != 1 and legend_flag:
        if len(experiments) < 4:
            shift_legend=len(max(legends))*.051 + 4.5/figx
            if len(custom_legends) == len(legends):
                ax.legend(legends,loc=7, bbox_to_anchor=(shift_legend, 0.5), fontsize=16)
            else:
                ax.legend(loc=7, bbox_to_anchor=(shift_legend, 0.5), fontsize=16)
        else:
            ax.legend(loc='right', fontsize=16, framealpha=0.5)
    else:
        ax.get_legend().remove()

    if custom_title != '':
        title = custom_title
    ax.set_title(title, fontsize=16)
    if save_img_path != None:
        plt.savefig(save_img_path + '/' + save_img_name, bbox_inches='tight', 
                    pad_inches=0.1, dpi=300)
    
    return plot_dict

def make_patch_spines_invisible(ax):
    ax.set_frame_on(False)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)
    ax.get_xaxis().set_visible(True)
    ax.set_yticks([]) 


def plot_dual_ax_bar_graph(adict, legends=None, legend_flag=True,
                           experiments=None, option=None,
                           option_exact=False, save_img_path=None,
                           save_img_name=None, width=0.9,
                           custom_legends=[],custom_xlabel=[],custom_ylabel='',
                           custom_title='',custom_colormap='', annotate=True,
                           custom_annotate=[],rotate=90,timeunit='min',
                           log_y=False,custom_anno_loc=0.0):
    '''
    input:
    adict: a dictionary from io.read_[functions]
    legends: specify which stats to print. e.g. ['ls=maxits', 'ls=brkdn']
             legends are case sensitive
    option: used only if legends is not specified. options are  case insensitive.
    experiments: enter a list of experiment names or it prints all experiments
    option_exact: option to plot has to have exact keyword or a part of keyword
    '''
    ### determine legends you want to plot
    # select one key as a reference

    legends=['Final_SNES_Time','Final_NI','Final_LI']

    plot_dict = {}
    for legend in legends:
        plot_dict[legend] = []
        
    xticks = []
    ylabel = ''
    
    for key in experiments:
        xticks.append(key)
        for legend in legends:
            if legend in adict[key]:
                plot_dict[legend].append(adict[key][legend])
            else:
                plot_dict[legend].append(0)
    
    if 'Final_SNES_Time' in legends:
        ylabel = adict[key]['Final_SNES_Time_Unit']
        if timeunit == 'min':
            for i in range(len(plot_dict['Final_SNES_Time'])):
                plot_dict['Final_SNES_Time'][i] = \
                    plot_dict['Final_SNES_Time'][i]/60.
            ylabel = 'Wall-clock Time [min]'
        if timeunit == 'hour':
            for i in range(len(plot_dict['Final_SNES_Time'])):
                plot_dict['Final_SNES_Time'][i] = \
                    plot_dict['Final_SNES_Time'][i]/(60.*60.)
            ylabel = 'Wall-clock Time [hour]'
    elif 'Final_Sim_Time' in legends:
        ylabel = adict[key]['Final_Sim_Time_Unit']
    elif 'Final_Wall_Time' in legends:
        ylabel = adict[key]['Final_Wall_Time_Unit']
    elif 'Fixed_Sim_Time' in legends:
        ylabel = adict[key]['Fixed_Sim_Time_Unit']
        
    title = legends[0]
    if len(custom_legends) == len(legends):
        legends = custom_legends
    if len(custom_xlabel) == len(experiments):
        experiments = custom_xlabel
    if custom_ylabel != '':
        ylabel = custom_ylabel
    
    print(legends)

    df = pd.DataFrame(plot_dict)
    figx = len(experiments)*1.4*(width+0.2)
    fig, ax = plt.subplots(figsize=(figx, 5))
    ax2 = ax.twinx()
    ax3 = ax.twinx()
    ax2.spines["left"].set_position(("axes", 0))
    ax2.yaxis.set_label_position("left")
    ax2.spines["left"].set_visible(True)
    make_patch_spines_invisible(ax)
    
    ax2_color='tab:blue'
    ax3_color='tab:red'
    
    # plot the bar first
    df['Final_SNES_Time'].plot(kind='bar', color='silver', width=width, 
                               ax=ax)
    # linear iteration tick
    df['Final_LI'].plot(kind='line', logy=log_y, linestyle='None', marker=8, 
                        color=ax2_color, ms=20, ax=ax2)
    # nonlinear iteration tick                        
    df['Final_NI'].plot(kind='line', logy=log_y, linestyle='None', marker=9, 
                        color=ax3_color, ms=20, ax=ax3)

    ax2.yaxis.label.set_color(ax2_color)
    ax3.yaxis.label.set_color(ax3_color)
    
    nbar = len(df['Final_LI'])
    i=0
    ax2.axhline(xmin=0, xmax=(i/nbar+.45*width/nbar), y=df['Final_LI'][i], 
                color=ax2_color, lw=1, ls=':',zorder=3,label='Linear')
    i=nbar-1
    ax3.axhline(xmin=i/nbar+.65*width/nbar, xmax=1, y=df['Final_NI'][i], 
                color=ax3_color, lw=1, ls=':',zorder=10,label='Noninear')
    
# determine min and max so the plots look good
    if log_y:
        li_min = df['Final_LI'].min()
        li_max = df['Final_LI'].max()
        ni_min = df['Final_NI'].min()
        ni_max = df['Final_NI'].max()
        li_logmin = np.floor(np.log10(li_min))
        li_logmax = np.ceil(np.log10(li_max))
        ni_logmin = np.floor(np.log10(ni_min))
        ni_logmax = np.ceil(np.log10(ni_max))
        
        li_diff = li_logmax - li_logmin
        ni_diff = ni_logmax - ni_logmin
    
        if li_diff > 1:
            ax2min = li_min/5
            ax2max = li_max*1.5
        else:
            ax2min = 10.**li_logmin/5
            ax2max = 10.**li_logmax*1.2
        if ni_diff > 1:
            ax3min = ni_min/5
            ax3max = ni_max*1.5
        else:
            ax3min = 10.**ni_logmin/5
            ax3max = 10.**ni_logmax*1.2
    
        ax2.set_ylim(ax2min,ax2max)
        ax3.set_ylim(ax3min,ax3max)
    else:
        li_min = df['Final_LI'].min()
        li_max = df['Final_LI'].max()
        ni_min = df['Final_NI'].min()
        ni_max = df['Final_NI'].max()
        
        li_range = li_max - li_min
        ni_range = ni_max - ni_min
        li_margin = li_range*.15
        ni_margin = ni_range*.15
        ax2min = li_min - li_margin*2.
        if ax2min < 0:
            ax2min = 0
        ax2max = li_max + li_margin
        ax3min = ni_min - ni_margin*2.
        if ax3min < 0:
            ax3min = 0
        ax3max = ni_max + ni_margin
        ax2.set_ylim(ax2min,ax2max)
        ax3.set_ylim(ax3min,ax3max)

    ax2.yaxis.tick_left()
    ax3.yaxis.tick_right()
    plt.xlim([-width*.65, len(df['Final_SNES_Time'])-width*.45])
    ax.set_xticklabels(xticks)

    # computation time annotation
    max_height = 0.0
    for i in range(nbar):
        if ax.patches[i].get_height() > max_height:
            max_height = ax.patches[i].get_height()
    first_line = max_height*(custom_anno_loc+0.1)
    second_line = max_height*(custom_anno_loc+0.04)
        #reset counter 
    i=0
    if annotate:
        for p in ax.patches:
            ax.annotate('%.1f'%p.get_height(), 
                        (p.get_x()+p.get_width()/2., first_line),
                        fontsize=14,ha='center',va='center',
                        weight='bold',color='#454545')
            if timeunit=='min':
                ax.annotate('mins', 
                        (p.get_x()+p.get_width()/2., second_line),
                        fontsize=14,ha='center',va='center',
                        weight='bold',color='#454545')
            else:
                ax.annotate('hours', 
                        (p.get_x()+p.get_width()/2., second_line),
                        fontsize=14,ha='center',va='center',
                        weight='bold',color='#454545')
            i+=1
            fig.texts.append(ax.texts.pop())

    # axis labels and font sizes
    ax.tick_params(axis='x', labelsize=16, which='major', length=0)
    ax2.tick_params(axis='y', labelsize=16, which='major', length=6)
    ax3.tick_params(axis='y', labelsize=16, which='major', length=6)
    ax2.tick_params(axis='y', which='minor', length=4, labelsize=12)
    ax3.tick_params(axis='y', which='minor', length=4, labelsize=12)
    ax2.tick_params(axis='y', which='both', colors=ax2_color)
    ax3.tick_params(axis='y', which='both', colors=ax3_color)
    # display some minor tick values.
    if log_y:
        ax2.get_yaxis().set_minor_formatter(ticker.FuncFormatter(ticks_format))
        ax3.get_yaxis().set_minor_formatter(ticker.FuncFormatter(ticks_format))
    else:
#        yfmt2 = ticker.ScalarFormatter(useMathText=True)
#        yfmt2.set_powerlimits((0,1))
#        yfmt3 = ticker.ScalarFormatter(useMathText=True)
#        yfmt3.set_powerlimits((0,1))
#        ax2.get_yaxis().set_major_formatter(yfmt2)
#        ax3.get_yaxis().set_major_formatter(yfmt3)
        ax2.get_yaxis().set_major_formatter(ticker.FuncFormatter(human_format))
        ax3.get_yaxis().set_major_formatter(ticker.FuncFormatter(human_format))
        ax2.yaxis.get_offset_text().set_fontsize(14)
        ax3.yaxis.get_offset_text().set_fontsize(14)
        max_yticks=5
        ynticks2 = ticker.MaxNLocator(max_yticks)
        ynticks3 = ticker.MaxNLocator(max_yticks)
        ax2.yaxis.set_major_locator(ynticks2)
        ax3.yaxis.set_major_locator(ynticks3)
    ax.set_xticklabels(experiments, rotation=rotate,ha="center")
    ax2.set_ylabel('Linear Iterations', fontsize=16)
    ax3.set_ylabel('Nonlinear Iterations', fontsize=16)

    # legend is not needed anymore    
    if len(legends) != 1 and legend_flag:
        if len(experiments) < 4:
            shift_legend=len(max(legends))*.051 + 4.5/figx
            if len(custom_legends) == len(legends):
                ax.legend(legends,loc=7, bbox_to_anchor=(shift_legend, 0.5), 
                          fontsize=16)
            else:
                ax.legend(loc=7, bbox_to_anchor=(shift_legend, 0.5), 
                          fontsize=16)
        else:
            ax.legend(loc='right', fontsize=16, framealpha=0.5)

    
    if custom_title != '':
        title = custom_title
    ax.set_title(title, fontsize=16)#, y=1.08)
    if save_img_path != None:
        plt.savefig(save_img_path + '/' + save_img_name, bbox_inches='tight', 
                    pad_inches=0.1, dpi=300)
    
    return plot_dict

         
def plot_line(adict, legends=None, legend_flag=True,
              experiments=None, option=None,
              logx=False, logy=False,
              option_exact=False, save_img_path=None,
              save_img_name=None, width=0.9,
              custom_legends=[],custom_xlabel=[],custom_ylabel='',
              custom_title='',custom_colormap='',
              custom_annotate=[]):
    '''
    input:
    adict: a dictionary from io.read_[functions]
    legends: specify which stats to print. e.g. ['ls=maxits', 'ls=brkdn']
             legends are case sensitive
    option: used only if legends is not specified. options are  case insensitive.
    experiments: enter a list of experiment names or it prints all experiments
    option_exact: option to plot has to have exact keyword or a part of keyword
    '''
    ### determine legends you want to plot
    # select one key as a reference

    refkeys = adict[list(adict.keys())[0]].keys()
    keylength = 0
    for i in adict.keys():
        if len(adict[i].keys()) > keylength:
            refkeys = adict[i].keys()
            keylength = len(adict[i].keys())

    if legends==None:
        legends = []
        if option == None:
            # generate legends for all keys available
            for key in refkeys:
                legends.append(key)
        else:
            for key in refkeys:
                if option_exact:
                    if option.lower() == key.lower():
                        legends.append(key)
                else:
                    if option.lower() in key.lower():
                        legends.append(key)
    else:
        # check and see if all requested keys exist
        for key in legends:
            if key in refkeys:
                continue
            else:
                print ("ERROR: the requested key '%s' does not exist in the" \
                      " dictionary provided. legends are Case Sensitve." \
                      " options are case insensitive." % key)
                return
    
    plot_dict = {}
    for legend in legends:
        plot_dict[legend] = []
    
    ### determine keys you want to plot
    if experiments == None:    
        experiments = sorted(adict.keys())
    else:
        # check and see if all requested keys exist
        for key in experiments:
            if key in adict.keys():
                continue
            else:
                print ("ERROR: the requested key '%s' does not exist in the"
                      " dictionary provided" % key)
                return
                
    xticks = []
    ylabel = ''

    for key in experiments:
        xticks.append(key)
        for legend in legends:
            if legend in adict[key]:
                plot_dict[legend].append(adict[key][legend])
            else:
                plot_dict[legend].append(0)
    
    if 'Final_SNES_Time' in legends:
        ylabel = adict[key]['Final_SNES_Time_Unit']
    elif 'Final_Sim_Time' in legends:
        ylabel = adict[key]['Final_Sim_Time_Unit']
    elif 'Final_Wall_Time' in legends:
        ylabel = adict[key]['Final_Wall_Time_Unit']
    elif 'Fixed_Sim_Time' in legends:
        ylabel = adict[key]['Fixed_Sim_Time_Unit']
        
        
    title = legends[0]
    if len(custom_legends) == len(legends):
        legends = custom_legends
    if len(custom_xlabel) == len(experiments):
        experiments = custom_xlabel
    if custom_ylabel != '':
        ylabel = custom_ylabel
    
    print(legends)
    figx = len(experiments)*1.5
    df = pd.DataFrame(plot_dict)
    if custom_colormap != '':
        cmap=custom_colormap
    else:
        cmap='Paired'
    ax = df.plot(kind='line', colormap=cmap, figsize=(figx,5))

    ax.tick_params(axis='x', labelsize=16)
    ax.tick_params(axis='y', labelsize=16)
    ax.set_xticklabels(experiments)
    ax.set_ylabel(ylabel, fontsize=16)
    if len(legends) != 1 and legend_flag:
        if len(experiments) < 4:
            shift_legend=len(max(legends))*.051 + 4.5/figx
            if len(custom_legends) == len(legends):
                ax.legend(legends,loc=7, bbox_to_anchor=(shift_legend, 0.5), fontsize=16)
            else:
                ax.legend(loc=7, bbox_to_anchor=(shift_legend, 0.5), fontsize=16)
        else:
            ax.legend(loc='right', fontsize=16, framealpha=0.5)
    else:
        ax.get_legend().remove()

    if custom_title != '':
        title = custom_title
    ax.set_title(title, fontsize=16)
    if save_img_path != None:
        plt.savefig(save_img_path + '/' + save_img_name, bbox_inches='tight', 
                    pad_inches=0.1)
    
    return plot_dict