#!/bin/env python
"""
Program to analyze screen output of PFLOTRAN.

@author: Heeho Park <heepark@sandia.gov>
"""

import sys
import os
import analyzer_io as io
import analyzer_post_process as pp

#path = '/home/heepark/models/wipp/2d/s1_r3_v078/'
#path = '/home/heepark/models/wipp/2d/s2_r3_v058/'
#path = '/home/heepark/models/wipp/2d/s3_r1_v077/'
#path = '/home/heepark/models/wipp/2d/s4_r2_v023/'
#path = '/home/heepark/models/wipp/2d/s5_r2_v001/'  #thesis
#path = '/home/heepark/models/wipp/2d/s6_r1_v040/'
#path = '/home/heepark/models/wipp/3d/s1_r3_v078'
#path = '/home/heepark/models/uz/small'
path = '/home/heepark/models/wipp/3d/s1_r3_v078'  #thesis
ext = 'stdout'

file_list, experiment_list = io.scanfolder(path,ext)
fixed_timestep, simulation_time = io.scan_screen_output(file_list)
fixed_timestep=500
sim_result = io.read_screen_output(file_list, fixed_timestep, simulation_time)
print(sim_result.keys())
print(sim_result[sim_result.keys()[0]].keys())
sim_cut_result = io.read_cut_reasons(file_list)
print(sim_cut_result[sim_cut_result.keys()[0]].keys())


#save_img_path = '/home/heepark/models/wipp/2d/s1_r3_v078/images'
#save_img_path = '/home/heepark/models/wipp/2d/s2_r3_v058/images'
#save_img_path = '/home/heepark/models/wipp/2d/s3_r1_v077/images'
#save_img_path = '/home/heepark/models/wipp/2d/s4_r2_v023/images'
#save_img_path = '/home/heepark/models/wipp/2d/s5_r2_v001/images'  #thesis
#save_img_path = '/home/heepark/models/wipp/2d/s6_r1_v040/images'
#save_img_path = '/home/heepark/models/wipp/3d/s1_r3_v078/images'
#save_img_path = '/home/heepark/models/uz/small/images'
#save_img_path = '/home/heepark/models/wipp/3d/s1_r3_v078/images'  #thesis

#save_img_path = None
#save_img_path = '/home/heepark/models/wipp/pflotran-bragflo-2d-flared/images/proposal'
#pp.plot_stacked_bar_graph(sim_cut_result,option='ls',
#                          experiments=['ilu0','ilu1','ilu2','ilu5'])
#pp.plot_stacked_bar_graph(sim_cut_result,option='snes',
#                          experiments=['ilu0','ilu1','ilu2','ilu5'])
#pp.plot_stacked_bar_graph(sim_result,option='final_wall_time',
#                          experiments=['ilu0','ilu1','ilu2','ilu5'],
#                          option_exact=True)
'''
cases = ['hypre_pilut_1e-6', 'hypre_pilut_1e-5_14_bcgs',
'hypre_pilut_1e-5_30_bcgs', 'hypre_pilut_1e-5_18_bcgs', 'default_gmres', 
'gmres_ilu1', 'hypre_pilut_1e-2_15_gmres', 
'hypre_pilut_1e-5_30_gmres', 'hypre_pilut_1e-4_30_gmres', 
'hypre_pilut_1e-4', 'hypre_pilut_1e-5', 'bcgs_ilu0', 
'hypre_pilut_1e-4_30_bcgs', 'hypre_pilut_1e-2_15_bcgs', 
'hypre_pilut_1e-2_24_gmres', 'hypre_pilut_1e-2_24_bcgs']
cases = sorted(cases)
'''

#pp.plot_stacked_bar_graph(sim_result,option='fixed_',
#                          experiments=cases,
#                          option_exact=True)
#pp.plot_stacked_bar_graph(sim_cut_result,option='lsfrac=',
#                          experiments=cases)
#pp.plot_stacked_bar_graph(sim_result,option='snes',
#                          experiments=cases,
#                          option_exact=True)

#cases = ['direct', 'bcgs-ilu0', 'bcgs-ilu1', 'gmres-ilu0', 'gmres-ilu1']
cases = ['direct', 'bcgs-ilu0', 'bcgs-ilu5']
#cases = ['direct', 'bcgs-ilu0']
# cases=sorted(cases)
'''
pp.plot_stacked_bar_graph(sim_result, option='final_cuts_per_ts',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='final_li_per_ni',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='final_ni_per_ts',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='final_ts',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='final_cuts',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='final_ni',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='final_li',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='final_wall_time',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_cut_result, option='ls_',
                          experiments=cases
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_cut_result, option='ni_',
                          experiments=cases
                          ,save_img_path=save_img_path)
'''
#cases = ['bcgs-ilu1-ts500-1core', 'bcgs-ilu1-ts500-4core', 
#         'gmres-ilu1-ts500-1core', 'gmres-ilu1-ts500-4core']
#cases = ['iterative2']
#cases = ['bcgs-ilu0-ts500-1core','bcgs-ilu0-ts500-4core',
#         'bcgs-ilu1-ts500-1core','bcgs-ilu1-ts500-4core',
#         'gmres-ilu0-ts500-1core','gmres-ilu0-ts500-4core',
#         'gmres-ilu1-ts500-1core','gmres-ilu1-ts500-4core']
#cases = experiment_list
'''
pp.plot_stacked_bar_graph(sim_result, option='fixed_cuts_per_ts',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='fixed_li_per_ni',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='fixed_ni_per_ts',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='fixed_ts',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='fixed_cuts',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='fixed_sim_time',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='fixed_ni',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_result, option='fixed_li',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_cut_result, option='ls_',
                          experiments=cases
                          ,save_img_path=save_img_path)
pp.plot_stacked_bar_graph(sim_cut_result, option='ni_',
                          experiments=cases
                          ,save_img_path=save_img_path)
'''

pp.plot_stacked_bar_graph(sim_cut_result, option='ni_',
                          experiments=cases,
                          save_img_path=save_img_path,
                          custom_legends=['Intentional TS Cut','Linear Solver Failure','Max Nonlinear Iter'],
                          custom_xlabel=['direct','bcgs-ilu0'],
                          custom_ylabel='Number of Time Step Cuts',
                          custom_title='Newton Solver Failure Reasons')

pp.plot_stacked_bar_graph(sim_cut_result, option='ls_',
                          experiments=cases
                          ,save_img_path=save_img_path,
                          custom_legends=['Exceeds Divergence Tol','Max Linear Iterations'],
                          custom_xlabel=['direct','bcgs-ilu0'],
                          #custom_ylabel='Number of Linear Solver Failure',
                          custom_title='Linear Solver Failure Reasons',
                          custom_colormap='jet')
'''
pp.plot_stacked_bar_graph(sim_result, option='final_wall_time',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path,
                          custom_title='Single-Core Wall Time')
pp.plot_stacked_bar_graph(sim_result, option='final_cuts',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path,
                          custom_title='Time Step Cuts')
pp.plot_stacked_bar_graph(sim_result, option='final_ts',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path,
                          custom_title='Time Steps')
pp.plot_stacked_bar_graph(sim_result, option='final_ni',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path,
                          custom_title='Newton Iterations')
'''
'''
pp.plot_stacked_bar_graph(sim_result, option='final_wall_time',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path,
                          custom_title='4-Core Wall Time')
pp.plot_stacked_bar_graph(sim_result, option='final_ts',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path,
                          custom_title='Time Steps')
pp.plot_stacked_bar_graph(sim_result, option='final_ni',
                          experiments=cases, option_exact=True
                          ,save_img_path=save_img_path,
                          custom_title='Newton Iterations')
pp.plot_stacked_bar_graph(sim_result, option='final_li_per_ni',
                          experiments=['bcgs-ilu0', 'bcgs-ilu5'], option_exact=True
                          ,save_img_path=save_img_path,
                          custom_title='Linear Iter/Newton Iter')
'''