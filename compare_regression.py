#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 15:17:26 2020

@author: heepark

Python scripts to compare multiple regression files.

"""

import os
import re
import textwrap
import pprint
import shutil
import subprocess
import datetime
import time
import sys


#failures
_NULL_FAILURE = 0
_MINOR_FAILURE = 1
_MAJOR_FAILURE = 2
#errors
_NULL_ERROR = 0
_GENERAL_ERROR = 1
_PFLOTRAN_USER_ERROR = 2
_PFLOTRAN_CRASH = 3
_PFLOTRAN_FAILURE = 4
_CONFIG_ERROR = 5
_MISSING_INFO_ERROR = 6
_PRE_PROCESS_ERROR = 7
_POST_PROCESS_ERROR = 8
_TIMEOUT_ERROR = 9

class TestStatus(object):
    """
    Simple class to hold status info.
    """
    def __init__(self):
        self.fail = 0
        self.warning = 0
        self.error = 0
        self.skipped = 0
        self.test_count = 0

    def __str__(self):
        message = "fail = {0}\n".format(self.fail)
        message += "warning = {0}\n".format(self.warning)
        message += "error = {0}\n".format(self.error)
        message += "skipped = {0}\n".format(self.skipped)
        message += "test_count = {0}\n".format(self.test_count)
        return message

class RegressionTest(object):
    """
    Class to collect data about a test problem, run the problem, and
    compare the results to a known result.
    """
    _wall_time_re = re.compile(r"Time \(seconds\)")

    def __init__(self):
        # define some constants
        self._ABSOLUTE = "absolute"
        self._RELATIVE = "relative"
        self._PERCENT = "percent"
        self._TIME = "time"
        self._CONCENTRATION = "concentration"
        self._GENERIC = "generic"
        self._DISCRETE = "discrete"
        self._RATE = "rate"
        self._VOLUME_FRACTION = "volume_fraction"
        self._PRESSURE = "pressure"
        self._SATURATION = "saturation"
        self._DISPLACEMENT = "displacement"
        self._STRAIN = "strain"
        self._STRESS = "stress"
        self._SOLUTION = "solution"
        self._RESIDUAL = "residual"
        self._MAJOR_SCALE = "major_scale"
        self._TOL_VALUE = 0
        self._TOL_TYPE = 1
        self._TOL_MIN_THRESHOLD = 2
        self._TOL_MAX_THRESHOLD = 3
        self._PFLOTRAN_SUCCESS = 86
        self._PFLOTRAN_USER_ERROR = 87
        self._PFLOTRAN_FAILURE = 88
        self._RESTART_PREFIX = "tmp-restart"
        # misc test parameters
        self._pprint = pprint.PrettyPrinter(indent=2)
        self._txtwrap = textwrap.TextWrapper(width=78, subsequent_indent=4*" ")
        self._debug = False
        self._executable = None
        self._input_arg = "-input_prefix"
        self._input_suffix = "in"
        self._np = None
        self._pflotran_args = None
        self._python_setup_script = None
        self._stochastic_realizations = None
        
        self._check_performance = False
        self._test_name = None
        
        self._tolerance = {}
        self._tolerance[self._TIME] = [5.0, self._PERCENT, \
                                       0.0, sys.float_info.max]
        self._tolerance[self._DISCRETE] = [0, self._ABSOLUTE, 0, sys.maxsize]
        # a failure is considered major if greater than 0.1% relative
        self._tolerance[self._MAJOR_SCALE] = [1.e6, self._RELATIVE, 
                                              0., sys.float_info.max]
        common = [self._CONCENTRATION, self._GENERIC, self._RATE, 
                  self._VOLUME_FRACTION, \
                  self._PRESSURE, self._SATURATION, self._RESIDUAL, \
                  self._DISPLACEMENT, self._STRESS, self._STRAIN]
        for t in common:
            self._tolerance[t] = [0.005, self._RELATIVE, \
                                  0.0, sys.float_info.max]
        
    def name(self):
        return self._test_name
    
    def compare_regression_files(self, current_filename, gold_filename, 
                                  status, testlog):
        """
        Test the output from the run against the known "gold standard"
        output and determine if the test succeeded or failed.

        We return zero on success, one on failure so that the test
        manager can track how many tests succeeded and failed.
        """
        if not os.path.isfile(gold_filename):
            message = self._txtwrap.fill(
                "ERROR: could not find regression test gold file "
                "'{0}'. If this is a new test, please create "
                "it with '--new-test'.".format(gold_filename))
            print("".join(['\n', message, '\n']), file=testlog)
            status.error = _MISSING_INFO_ERROR
            return
        else:
            with open(gold_filename, 'rU') as gold_file:
                gold_output = gold_file.readlines()

        if not os.path.isfile(current_filename):
            message = self._txtwrap.fill(
                "ERROR: could not find regression test file '{0}'."
                " Please check the standard output file for "
                "errors.".format(current_filename))
            print("".join(['\n', message, '\n']), file=testlog)
            status.error = _MISSING_INFO_ERROR
            return
        else:
            with open(current_filename, 'rU') as current_file:
                current_output = current_file.readlines()

        print("    diff {0} {1}".format(gold_filename, current_filename),
              file=testlog)

        gold_sections = self._get_sections(gold_output)
        current_sections = self._get_sections(current_output)

        errors = []
        num_minor_fail = 0
        num_major_fail = 0

        if self._debug:
            print("--- Gold sections:")
            self._pprint.pprint(gold_sections)
            print("--- Current sections:")
            self._pprint.pprint(current_sections)

        # look for sections that are in gold but not current
        for section in gold_sections:
            if section not in current_sections:
                errors.append(_MISSING_INFO_ERROR)
                print("    ERROR: section '{0}' is in the gold output, but "
                      "not the current output.".format(section), 
                      file=testlog)

        # look for sections that are in current but not gold
        for section in current_sections:
            if section not in gold_sections:
                errors.append(_MISSING_INFO_ERROR)
                print("    ERROR: section '{0}' is in the current output, "
                      "but not the gold output.".format(section), 
                      file=testlog)

        # compare common sections
        for section in gold_sections:
            if section in current_sections:
                try:
                    report = self._compare_sections(gold_sections[section],
                                           current_sections[section],
                                           testlog)
                    num_minor_fail += report[0]
                    num_major_fail += report[1]
                    if not report[2] == _NULL_ERROR:
                        errors.append(report[2])
                except Exception as error:
                    status.error = _CONFIG_ERROR
                    print(error, file=testlog)

        if num_minor_fail > 0:
            status.fail = _MINOR_FAILURE
        if num_major_fail > 0:
            status.fail = _MAJOR_FAILURE
        # check to ensure all errors are identical, otherwise, throw
        # a general error
        if len(errors) > 0:
            status.error = errors[0]
            for i in range(1,len(errors)):
                if status.error == errors[1]:
                    status.error = _GENERAL_ERROR
                    break

    def _get_sections(self, output):
        """
        Get the sections from the regression file.

        Each section in the regression test file looks like:
        -- TYPE: NAME --
           key: value
           key: value
        """
        name_re = re.compile(r"^--[\s]+([\w]+):(.*)[\s]+--$")
        sections = {}
        sect = {}
        for line in output:
            match = name_re.match(line)
            if match:
                # save the old section, if any
                if 'name' in sect:
                    sections[sect['name']] = sect
                name = match.group(2).strip()
                data_type = match.group(1)
                sect = {}
                sect['name'] = name
                sect['type'] = data_type
            else:
                temp = line.split(':')
                name = temp[0].strip()
                value = temp[1].strip()
                sect[name] = value
        # add the final section
        if 'name' in sect:
            sections[sect['name']] = sect

        return sections

    def _compare_sections(self, gold_section, current_section, testlog):
        """
        Compare the fields of the current section.
        """
        if gold_section["name"] != current_section["name"]:
            raise RuntimeError("ERROR: an internal error occured. "
                               "compare_sections receive different sections. "
                               "gold = '{0}' current = '{1}'".format(
                                   gold_section["name"], current_section["name"]))
        name = gold_section['name']
        data_type = gold_section['type']
        section_num_minor_fail = 0
        section_num_major_fail = 0
        section_num_error = 0
        section_status = 0
        if self._check_performance is False and data_type.lower() == self._SOLUTION:
            # solution blocks contain platform dependent performance
            # metrics. We skip them unless they are explicitly
            # requested.
            #print("    Skipping {0} : {1}".format(data_type, name), file=testlog)
            print("No Performance Check")
        else:
            # if key in gold but not in current --> failed test
            for k in gold_section:
                if k not in current_section:
                    section_num_error += 1
                    print("    ERROR: key '{0}' in section '{1}' found in gold "
                          "output but not current".format(
                              k, gold_section['name']), file=testlog)

            # if key in current but not gold --> failed test
            for k in current_section:
                if k not in gold_section:
                    section_num_error += 1
                    print("    ERROR: key '{0}' in section '{1}' found in current "
                          "output but not gold".format(k, current_section['name']),
                          file=testlog)

            # now compare the keys that are in both...
            for k in gold_section:
                if k == "name" or k == 'type':
                    pass
                elif k in current_section:
                    name_str = name + ":" + k
                    # the data may be vector
                    gold = gold_section[k].split()
                    current = current_section[k].split()
                    if len(gold) != len(current):
                        section_num_error += 1
                        print("    ERROR: {0} : {1} : vector lengths not "
                              "equal. gold {2}, current {3}".format(
                                  name, k, len(gold), len(current)), file=testlog)
                    else:
                        for i in range(len(gold)):
                            try:
                                report = self._compare_values(name_str, 
                                             data_type, gold[i], current[i], 
                                             testlog)
                                section_num_minor_fail += report[0]
                                section_num_major_fail += report[1]
                            except Exception as error:
                                section_status += 1
                                print("ERROR: {0} : {1}.\n  {2}".format(
                                    self.name(), k, str(error)), file=testlog)

        if False:
            print("    {0} : status : {1}".format(
                name, section_status), file=testlog)

        return section_num_minor_fail, section_num_major_fail, \
               section_num_error

    def _compare_values(self, name, key, previous, current, testlog):
        """
        Compare the values using the appropriate tolerance and criteria.

        NOTE(bja): previous and current come into this function as
        strings. We don't know if they should be floats or ints (or
        possibly strings) until we know what the data type is. For
        'discrete' or 'solution' variables, we have to do further work
        to figure it out!
        """
        num_minor_fail = 0
        num_major_fail = 0
        tol = None
        key = key.lower()
        if (key == self._CONCENTRATION or
            key == self._GENERIC or
            key == self._RATE or
            key == self._VOLUME_FRACTION or
            key == self._PRESSURE or
            key == self._SATURATION or
            key == self._DISPLACEMENT or
            key == self._STRAIN or
            key == self._STRESS):
            previous = float(previous)
            current = float(current)
            tol = self._tolerance[key]
        elif key.lower() == self._SOLUTION:
            previous, current, tol = \
                self._compare_solution(name, previous, current)
        elif key.lower() == self._DISCRETE:
            previous, current, tol = \
                self._compare_discrete(name, previous, current)
        else:
            raise RuntimeError(
                "WARNING: the data caterogy '{0}' for '{1}' is not a known "
                "data category.".format(key, name))

        tolerance_type = tol[self._TOL_TYPE]
        tolerance = tol[self._TOL_VALUE]
        min_threshold = tol[self._TOL_MIN_THRESHOLD]
        max_threshold = tol[self._TOL_MAX_THRESHOLD]

        if tolerance_type == self._ABSOLUTE:
            delta = abs(previous - current)
        elif (tolerance_type == self._RELATIVE or
              tolerance_type == self._PERCENT):
            if previous != 0:
                delta = abs((previous - current) / previous)
            elif current != 0:
                delta = abs((previous - current) / current)
            else:
                # both are zero
                delta = 0.0
            if tolerance_type == self._PERCENT:
                delta *= 100.0
        else:
            # should never get here....
            raise RuntimeError("ERROR: unknown test tolerance_type '{0}' for "
                               "variable '{1}, {2}.'".format(tolerance_type,
                                                          name, key))

        if (previous < 1.e-10 and current < 1.e-10):
            # ignore small numbers - hdp
            delta = 0.0

        # base comparison to threshold on previous (gold) because it
        # is the known correct value!
        if min_threshold <= abs(previous) and abs(previous) <= max_threshold:

            if delta > tolerance:
                num_minor_fail += 1
                
                # check for major failure
                if delta > tolerance * \
                        self._tolerance[self._MAJOR_SCALE][self._TOL_VALUE]:
                    num_major_fail += 1

            if num_major_fail > 0:
                print("    MAJOR FAIL: {0} : {1} > {2} [{3}]".format(
                    name, delta, tolerance, tolerance_type), file=testlog)
                print("        Values: {0} and {1}".format(previous, current), 
                    file=testlog)
            elif num_minor_fail > 0:
                print("    FAIL: {0} : {1} > {2} [{3}]".format(
                    name, delta, tolerance, tolerance_type), file=testlog)
                print("        Values: {0} and {1}".format(previous, current), 
                    file=testlog)

            elif self._debug:
                print("    PASS: {0} : {1} <= {2} [{3}]".format(
                    name, delta, tolerance, tolerance_type), file=testlog)
                print("        Values: {0} and {1}".format(previous, current), 
                    file=testlog)

        else:
            # revert major failure if outside range
            num_major_fail = 0
            print("    SKIP: {0} : gold value ({1}) outside threshold range: "
                  "{2} <= abs(value) <= {3}".format(
                      name, previous, min_threshold, max_threshold),
                  file=testlog)

        return num_minor_fail, num_major_fail

    def _compare_solution(self, name, previous, current):
        """
        Tolerances for the solution variables depend on what field we are
        testing.

        """
        section = name.split(':')[0]
        param = name.split(':')[1]
        param = param.strip()
        if param == "Time (seconds)":
            previous = float(previous)
            current = float(current)
            tolerance = self._tolerance[self._TIME]
        elif param == "Time Steps":
            previous = int(previous)
            current = int(current)
            tolerance = self._tolerance[self._DISCRETE]
        elif param == "Newton Iterations":
            previous = int(previous)
            current = int(current)
            tolerance = self._tolerance[self._DISCRETE]
        elif param == "Solver Iterations":
            previous = int(previous)
            current = int(current)
            tolerance = self._tolerance[self._DISCRETE]
        elif param == "Time Step Cuts":
            previous = int(previous)
            current = int(current)
            tolerance = self._tolerance[self._DISCRETE]
        elif param == "Solution 2-Norm":
            previous = float(previous)
            current = float(current)
            tolerance = self._tolerance[self._GENERIC]
        elif param == "Residual 2-Norm":
            previous = float(previous)
            current = float(current)
            tolerance = self._tolerance[self._RESIDUAL]
        else:
            raise RuntimeError("ERROR: unknown variable '{0}' in solution "
                               "section '{1}'".format(param, section))

        return previous, current, tolerance
    
    def _compare_discrete(self, name, previous, current):
        """
        Get tolerances for a "discrete" section, e.g. Material IDs.

        Discrete values are integers, except when we are
        looking at the mean of a discrete variable. Then we
        may (probably) have a floating point value!
        """
        mean_re = re.compile("Mean")
        have_mean = mean_re.search(name)
        if not have_mean:
            # previous and current must both be ints...
            try:
                previous = int(previous)
            except ValueError:
                raise RuntimeError(
                    "ERROR: discrete gold value must be an int: '{0}' = {1}.".format(
                        name, previous))
            try:
                current = int(current)
            except ValueError:
                raise RuntimeError(
                    "ERROR: discrete current value must be an int: '{0}' = {1}.".format(
                        name, current))

            tolerance = self._tolerance[self._DISCRETE]
        else:
            previous = float(previous)
            current = float(current)
            tolerance = self._tolerance[self._GENERIC]

        return previous, current, tolerance


def append_command_to_log(command, testlog, tempfile):
    """
    Append the results of a shell command to the test log
    """
    print("$ {0}".format(" ".join(command)), file=testlog)
    testlog.flush()
    with open(tempfile, "w") as tempinfo:
        subprocess.call(command, shell=False,
                        stdout=tempinfo,
                        stderr=subprocess.STDOUT)
        # NOTE(bja) 2013-06 : need a short sleep to ensure the
        # contents get written...?
        time.sleep(0.01)
    with open(tempfile, 'r') as tempinfo:
        shutil.copyfileobj(tempinfo, testlog)

def setup_testlog(txtwrap,f1='',f2=''):
    """
    Create the test log and try to add some useful information about
    the environment, petsc and pflotran.
    """
    now = datetime.datetime.today().strftime("%Y-%m-%d_%H-%M-%S")
    filename = "pflotran-tests-{0}.testlog".format(now)
    if f1 != '' and f2 !=f'':
        filename = f1+'+'+f2+'.testlog'
    testlog = open(filename, 'w')
    
    print("\nTest log file : {0}".format(filename))

    print("PFLOTRAN Regression Test Log", file=testlog)
    print("Date : {0}".format(now), file=testlog)
    print("System Info :", file=testlog)
    print("    platform : {0}".format(sys.platform), file=testlog)
    test_dir = os.getcwd()
    print("Test directory : ", file=testlog)
    print("    {0}".format(test_dir), file=testlog)

    tempfile = "{0}/tmp-pflotran-regression-test-info.txt".format(test_dir)

    print("\nPFLOTRAN repository status :", file=testlog)
    print("----------------------------", file=testlog)
    print("\n\n", file=testlog)

    print("PETSc information :", file=testlog)
    print("-------------------", file=testlog)
    petsc_dir = os.getenv("PETSC_DIR", None)
    if petsc_dir:
        message = txtwrap.fill(
            "* WARNING * This information may be incorrect if you have more "
            "than one version of petsc installed.\n")
        print(message, file=testlog)
        print("    PETSC_DIR : {0}".format(petsc_dir), file=testlog)
        petsc_arch = os.getenv("PETSC_ARCH", None)
        if petsc_arch:
            print("    PETSC_ARCH : {0}".format(petsc_arch), file=testlog)

        os.chdir(petsc_dir)

        os.chdir(test_dir)
        print("\n\n", file=testlog)
    else:
        print("    PETSC_DIR was not defined.", file=testlog)

    if os.path.isfile(tempfile):
      os.remove(tempfile)
    return testlog

#base_dir = '/home/heepark/models/speed_test/skybridge/'
#tests = ['t1_easy']
#tests = ['t3_mid']
#tests = ['t5_hard']
#tests = ['t6_hard_fca_ntrdc']
#tests = ['t1_easy_gca_ntrdc_32']
#base_dir = '/home/heepark/models/speed_test/jt/'
#tests = ['t2_easy_fcq']
#tests = ['t2_easy_fca']
#tests = ['t1_easy']
#tests = ['t2_mid']

#base_dir = '/home/heepark/models/uz/debug_models/cube_99_sat/'
base_dir = '/home/heepark/models/uz/debug_models/cube_liq_phase/'
#base_dir = '/home/heepark/models/uz/debug_models/cube_two_phase/'
#base_dir = '/home/heepark/models/wipp/toy/'
tests = ['nt']
#base_dir = '/home/heepark/models/uz/boca/all_drifts_v_coarse_trial1/'
#base_dir = '/home/heepark/models/uz/boca/all_drifts_v_coarse_trial1_rescue/'
#base_dir = '/home/heepark/models/uz/boca/no_lgr_trial1_completed/'
#base_dir = '/home/heepark/models/uz/boca/no_lgr_trial1_rescue_completed/'
#tests = ['v1_NRF_24pwr']
#tests = ['v1_NRF_37pwr']
#tests = ['v1_nrf_12pwr']
#tests = ['v1_nrf_24pwr']
#tests = ['v1_nrf_37pwr']

test_dict =  {}
for test in tests:
    test_dict[test] = []
    test_dict[test+'_file'] = []
    for dir_name in os.listdir(base_dir):
        if test in dir_name:
            for file in os.listdir(base_dir + '/' + dir_name):
                if file.endswith(".regression"):
                    test_dict[test].append(dir_name)
                    test_dict[test+'_file'].append(file)

status = TestStatus()
txtwrap = textwrap.TextWrapper(width=78, subsequent_indent=4*" ")
test_reg = RegressionTest()
for test in tests:
    test_dict[test].sort()
    benchmark = base_dir + test_dict[test][0] + '/' + test_dict[test+'_file'][0]
    for i in range(len(test_dict[test])):
        file2 = base_dir + test_dict[test][i] + '/' + test_dict[test+'_file'][i]
        testlog = setup_testlog(txtwrap,f1=test_dict[test][0],f2=test_dict[test][i])
        test_reg.compare_regression_files(benchmark,file2,status,testlog)
        if status.fail == _MAJOR_FAILURE:
            print("Failed: "+test_dict[test][0]+" "+test_dict[test][i])
        testlog.close()

