#!/bin/env python
"""
Program to analyze screen output of PFLOTRAN.

@author: Heeho Park <heepark@sandia.gov>
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker

import analyzer_io as io
import analyzer_post_process as pp

path = '/home/heepark/models/speed_test/skybridge_rerun'
ext = 'stdout'

file_list, experiment_list = io.scanfolder(path,ext)
fixed_timestep, simulation_time = io.scan_screen_output(file_list)
sim_result = io.read_screen_output(file_list, fixed_timestep, simulation_time)
sim_cut_result = io.read_cut_reasons(file_list)
print (sim_result.keys())
print (sim_result[list(sim_result.keys())[0]].keys())
print (sim_cut_result[list(sim_cut_result.keys())[0]].keys())

save_img_path = '/home/heepark/models/speed_test/image'

def human_format(num, pos):
    magnitude = 0
    while abs(num) >= 1000:
        magnitude += 1
        num /= 1000.0
    # add more suffixes if you need them
    return '%3g%s' % (num, ['', 'K', 'M', 'G', 'T', 'P'][magnitude])


#%%
best_combo = ['t1_easy_dbin_32', 't1_easy_fcq_nt_32', 't1_easy_fca_nt_32']
pp.plot_dual_ax_bar_graph(sim_result, option='Final_SNES_Time',
                          experiments=best_combo,
                          save_img_path=save_img_path,
                          save_img_name='linear_solver_all',
                          custom_xlabel=['BCGS-ILU',
                                         'FGMRES-CPR-QIMPES',
                                         'FGMRES-CPR-ABF'],
                          legend_flag=False, timeunit='hour',
                          custom_anno_loc=0.4,
                          custom_title='Overall Computation Time\n'+
                                       'Newton Easy Case 32 cores')

pp.plot_stacked_bar_graph(sim_cut_result, option=None,
                          legends=['snes=-3','snes=-5','snes=-88'],
                          experiments=best_combo,
                          save_img_path=save_img_path,
                          save_img_name='linear_solver_all_cuts',
                          custom_legends=['Linear Solver Fail',
                                         'Max Nonlinear Iter',
                                         'Intentional TS Cut'],
                          custom_xlabel=['BCGS-ILU',
                                         'FGMRES-CPR-QIMPES',
                                         'FGMRES-CPR-ABF'],
                          custom_ylabel='# of TS Cuts',
                          custom_colormap='jet',
                          annotate=False,
                          legend_flag=True, timeunit='hour',
                          custom_title='Number  of Time Step Cuts\n'+
                                        'Categorized Reasons')

#%%
best_combo = ['t3_mid_fca_nt_32', 't3_mid_fca_ntrdc_32',
              't3_mid_fca_ntrdc_32_auto']
pp.plot_dual_ax_bar_graph(sim_result, option='final_snes_time',
                          experiments=best_combo, option_exact=True,
                          save_img_path=save_img_path,
                          save_img_name='t3_auto_scale_time',
                          custom_xlabel=['NT','NTRDC','NTRDC-AS'],
                          legend_flag=False, timeunit='hour',
                          #custom_anno_loc=0.3,
                          custom_title='Overall Computation Time FGMRES-ABF\n'+
                                       'Newton Mid Case 32 cores')


#%%
best_combo = ['t6_hard_fca_nt','t6_hard_fca_ntrdc','t6_hard_fca_ntrdc_auto']

pp.plot_dual_ax_bar_graph(sim_result, option='final_ts',
                          experiments=best_combo, option_exact=True,
                          save_img_path=save_img_path,
                          save_img_name='t6_auto_scale_time',
                          custom_xlabel=['NT','NTRDC',
                                         'NTRDC-AS'],
                          #custom_anno_loc=0.18,
                          legend_flag=False, timeunit='hour',
                          custom_title='Overall Computation Time FGMRES-ABF\n'+
                                       'Newton Hard Case 32 cores')


#%%
best_combo = ['t3_mid_bcgs_ilu_nt_64', 't3_mid_bcgs_ilu_ntrdc_64_auto']
pp.plot_dual_ax_bar_graph(sim_result, option='final_snes_time',
                          experiments=best_combo, option_exact=True,
                          save_img_path=save_img_path,
                          save_img_name='t3_auto_scale_time_bcgs',
                          custom_xlabel=['Newton','NTRDC-AS'],
                          legend_flag=False, timeunit='hour',
                          custom_anno_loc=0.3,
                          custom_title='Overall Computation Time \n'+
                                       'BCGS-ILU Mid Case 64 cores')


pp.plot_stacked_bar_graph(sim_cut_result, option=None,
                          legends=['snes=-3','snes=-5','snes=-88'],
                          experiments=best_combo,
                          save_img_path=save_img_path,
                          save_img_name='t3_linear_solver_all_cuts',
                          custom_legends=['Linear Solver Fail',
                                         'Max Nonlinear Iter',
                                         'Intentional TS Cut'],
                          custom_xlabel=['Newton','NTRDC-AS'],
                          custom_ylabel='# of TS Cuts',
                          custom_colormap='jet',
                          annotate=False,
                          legend_flag=True, timeunit='hour',
                          custom_title='Number  of Time Step Cuts\n'+
                                        'Categorized Reasons')

#%%
best_outcome = ['t1_easy_dbin_32', 't1_easy_gca_ntrdc_32']
pp.plot_dual_ax_bar_graph(sim_result, option='Final_SNES_Time',
                          experiments=best_outcome,
                          save_img_path=save_img_path,
                          save_img_name='linear_solver_best',
                          custom_xlabel=['BCGS-ILU-NEWTON',
                                         'GMRES-CPR-ABF-NTRDC-AS'],
                          legend_flag=False, timeunit='hour',
                          custom_anno_loc=0.25,
                          custom_title='Overall Computation Time\n'+
                                       'Easy Case 32 cores')

#%%
best_outcome = ['t3_mid_bcgs_ilu_nt_64', 't3_mid_fca_ntrdc_64']
pp.plot_dual_ax_bar_graph(sim_result, option='Final_SNES_Time',
                          experiments=best_outcome,
                          save_img_path=save_img_path,
                          save_img_name='linear_solver_best_mid',
                          custom_xlabel=['BCGS-ILU-NEWTON',
                                         'FGMRES-CPR-ABF-NTRDC-AS'],
                          legend_flag=False, timeunit='hour',
                          custom_anno_loc=0.25,
                          custom_title='Overall Computation Time\n'+
                                       'Mid Case 64 cores')

#%%
best_outcome = ['t3_mid_bcgs_ilu_nt_64', 't3_mid_fca_nt_64']
pp.plot_dual_ax_bar_graph(sim_result, option='Final_SNES_Time',
                          experiments=best_outcome,
                          save_img_path=save_img_path,
                          save_img_name='linear_solver_nt_mid',
                          custom_xlabel=['BCGS-ILU',
                                         'FGMRES-CPR-ABF'],
                          legend_flag=False, timeunit='hour',
                          custom_anno_loc=0.25,
                          custom_title='Overall Computation Time\n'+
                                       'Newton Mid Case 64 cores')

best_combo = ['t6_hard_fca_nt']

pp.plot_dual_ax_bar_graph(sim_result, option='final_ts',
                          experiments=best_combo, option_exact=True,
                          save_img_path=save_img_path,
                          save_img_name='linear_solver_nt_hard',
                          custom_xlabel=['FGMRES-CPR-ABF'],
                          #custom_anno_loc=0.18,
                          legend_flag=False, timeunit='hour',
                          custom_title='Overall Computation Time \n'+
                                       'Newton Hard Case 32 cores')

#%%

node_packing = ['t3_mid_fca_nt_16','t3_mid_fca_nt_02nodes',
                't3_mid_fca_nt_04nodes4',
                't3_mid_fca_nt_08nodes','t3_mid_fca_nt_16nodes']
np_time = []
for i in range(len(node_packing)):
    np_time.append(sim_result[node_packing[i]]['Final_SNES_Time'])

x = [0,1,2,3,4]

fig, ax1 = plt.subplots()
ax1.plot(x, np_time, marker='v',lw=1.8,ms=12,mew=1.5,
         linestyle='-',fillstyle='none',label='Different cpn')
ideal_line = np_time[3]*np.ones(len(x))
ax1.plot(x, ideal_line, 'y--',label='Ideal')
ax1.minorticks_off()
ax1.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
ax1.set_xticks(x)
ax1.set_xticklabels(['1 n\n16 cpn', '2 n\n8 cpn', '4 n\n4 cpn', '8 n\n2 cpn',
                     '16 n\n1 cpn'])
ax1.tick_params(axis='x', labelsize=16)
ax1.tick_params(axis='y', labelsize=16)
yfmt = ticker.ScalarFormatter(useMathText=True)
yfmt.set_powerlimits((0,1))
ax1.get_yaxis().set_major_formatter(yfmt)
ax1.set_ylabel('Wall-clock Time [sec]', fontsize=16)
ax1.set_title('Node Packing (node, cores per node)', fontsize=16, y=1.08)
ax1.yaxis.get_offset_text().set_fontsize(14)
ax1.set_ylim([10000,15000])

plt.legend(loc='upper right', fontsize=14, frameon=False)
plt.savefig(save_img_path + '/' + 'Node Packing', bbox_inches='tight', 
                    pad_inches=0.1, dpi=300)


#%%
ilu = ['t1_easy_dbin_32','t1_easy_dbin_64','t1_easy_dbin_28']
cpr = ['t1_easy_fca_nt_32','t1_easy_fca_nt_64','t1_easy_fca_nt_28']
cores = np.array([32,64,128])
options=['Final_SNES_Time','Final_NI']

ilu_time = []
ilu_ni = []
cpr_time = []
cpr_ni = []

for i in range(len(ilu)):
    ilu_time.append(sim_result[ilu[i]][options[0]])
    cpr_time.append(sim_result[cpr[i]][options[0]])
    ilu_ni.append(sim_result[ilu[i]][options[1]])
    cpr_ni.append(sim_result[cpr[i]][options[1]])

ilu_time = np.array(ilu_time)
ilu_ni = np.array(ilu_ni)
cpr_time = np.array(cpr_time)
cpr_ni = np.array(cpr_ni)

fig, ax1 = plt.subplots()
ax1color = 'magenta'
ax1.loglog(cores, ilu_time,
          color=ax1color,marker='v',lw=1.8,ms=12,mew=1.5,
          linestyle='-',fillstyle='none',label='Time: BCGS-ILU')
ax1.loglog(cores, cpr_time,
          color=ax1color,marker='o',lw=1.8,ms=12,mew=1.5,
          linestyle='-.',fillstyle='none',label='Time: FGMRES-CPR-ABF')
ax1.minorticks_off()
ax1.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
ax1.set_xticks(cores)
ax1.set_yticks([1000,10000,100000])
ax1.tick_params(axis='x', labelsize=16)
ax1.tick_params(axis='y', labelcolor=ax1color, labelsize=16)
ax1.set_ylabel('Wall-clock Time [sec]', fontsize=16, color=ax1color)
ax1.set_xlabel('Number of Cores', fontsize=16)
ax1.set_title('Effects of Domain Decomposition Easy Case', fontsize=16)
plt.legend(bbox_to_anchor=(-0.15, -0.2), loc='upper left', fontsize=14,
           frameon=False)

ax2 = ax1.twinx()
ax2color = 'blue'
ax2.semilogx(cores, ilu_ni,
          color=ax2color,marker='v',lw=1.8,ms=12,mew=1.5,
          linestyle='-',fillstyle='none',label='Iterations: BCGS-ILU')
ax2.semilogx(cores, cpr_ni,
          color=ax2color,marker='o',lw=1.8,ms=12,mew=1.5,
          linestyle='-.',fillstyle='none',label='Iterations: FGMRES-CPR-ABF')
ax2.minorticks_off()
ax2.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
ax2.set_xticks(cores)
ax2.tick_params(axis='y', labelcolor=ax2color, labelsize=16)
ax2.get_yaxis().set_major_formatter(ticker.FuncFormatter(human_format))
ax2.set_ylabel('Nonlinear Iterations', fontsize=16, color=ax2color)
ax2.set_ylim([0,110000])
plt.legend(bbox_to_anchor=(0.55, -0.2), loc='upper left', fontsize=14,
           frameon=False)
plt.savefig(save_img_path + '/' + 'Preconditioners Easy Case', bbox_inches='tight', 
                    pad_inches=0.1, dpi=300)

#%%
scale = ['t3_mid_fca_nt_01', 't3_mid_fca_nt_02', 't3_mid_fca_nt_04', 
         't3_mid_fca_nt_08',
         't3_mid_fca_nt_16', 't3_mid_fca_nt_32', 't3_mid_fca_nt_64',
         #'t3_mid_fca_ntrd_01',
         't3_mid_fca_ntrd_02', 't3_mid_fca_ntrd_04', 
         't3_mid_fca_ntrd_08',
         't3_mid_fca_ntrd_16', 't3_mid_fca_ntrd_32', 't3_mid_fca_ntrd_64',
         't3_mid_fca_ntrdc_01', 't3_mid_fca_ntrdc_02', 't3_mid_fca_ntrdc_04', 
         't3_mid_fca_ntrdc_08',
         't3_mid_fca_ntrdc_16', 't3_mid_fca_ntrdc_32', 't3_mid_fca_ntrdc_64']
#         ,'t3_mid_fcq_nt_16', 't3_mid_fcq_nt_32','t3_mid_fcq_nt_64']
option='Final_SNES_Time'
#methods=['fca_nt_','fca_ntrd_','fca_ntrdc_']
methods=['fca_nt_','fca_ntrdc_']

#methods_name=['Newton','NTR','NTRDC']
methods_name=['Newton','NTRDC']
scale_no_packing = ['t3_mid_fca_nt_04nodes4',#'t3_mid_fca_ntrd_04nodes4',
                    't3_mid_fca_ntrdc_16nodes']
scale_no_packing2 = ['t3_mid_fca_nt_08nodes4',#'t3_mid_fca_ntrd_08nodes4',
                     't3_mid_fca_ntrdc_08nodes4']
scale_no_packing3 = ['t3_mid_fca_nt_16nodes4',#'t3_mid_fca_ntrd_16nodes4',
                     't3_mid_fca_ntrd_16nodes4']
scale_no_packing4 = ['t3_mid_fca_nt_04',#'t3_mid_fca_ntrd_04',
                     't3_mid_fca_ntrdc_04']

scale_dict = {}
for i in range(len(methods)):
    scale_dict[methods[i]] = []
    cores_name = methods[i]+'cores' 
    scale_dict[cores_name] = []
    eff_name = methods[i]+'eff'
    scale_dict[eff_name] = []
    for j in range(len(scale)):
        if methods[i] in scale[j]:
            scale_dict[methods[i]].append(sim_result[scale[j]][option])
            scale_dict[cores_name].append(int(scale[j][-2:]))
            eff = 1./(scale_dict[methods[i]][-1]/scale_dict[methods[i]][0])/ \
                  (scale_dict[cores_name][-1]/scale_dict[cores_name][0])*100.
            scale_dict[eff_name].append(eff)
    scale_dict[methods[i]] = np.array(scale_dict[methods[i]])
    scale_dict[cores_name] = np.array(scale_dict[cores_name])

colors=['magenta','blue','green','red','lime','green','cyan',
        'fuchsia','aqua']
lines=['-','-.','-','-.','-.','-','-.','--','-']
markers=['','','v','x','*','x','o','^']
markers2=['o','^','v','s','>']
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
for i in range(len(methods)):
    ax.loglog(scale_dict[methods[i]+'cores'], scale_dict[methods[i]],
              color=colors[i],marker=markers[i],lw=1.8,ms=6,
              linestyle=lines[i],label=methods_name[i])
#label2=['Newton 4 cores\nper node (cpn)','NTR 4 cpn','NTRDC 4 cpn']
label2=['Newton 4 cores\nper node (cpn)','NTRDC 4 cpn']
for i in range(len(methods)):
    ax.loglog(16, sim_result[scale_no_packing[i]][option],
              color=colors[i],marker=markers2[i],lw=1.8,ms=12,mew=1.5,
              linestyle='none',fillstyle='none',
              label=label2[i])
for i in range(len(methods)):
    ax.loglog(32, sim_result[scale_no_packing2[i]][option],
              color=colors[i],marker=markers2[i],lw=1.8,ms=12,mew=1.5,
              linestyle='none',fillstyle='none')
for i in range(len(methods)):
    ax.loglog(64, sim_result[scale_no_packing3[i]][option],
              color=colors[i],marker=markers2[i],lw=1.8,ms=12,mew=1.5,
              linestyle='none',fillstyle='none')
for i in range(len(methods)):
    ax.loglog(4, sim_result[scale_no_packing4[i]][option],
              color=colors[i],marker=markers2[i],lw=1.8,ms=12,mew=1.5,
              linestyle='none',fillstyle='none')
    
ax.loglog([64], 
          [sim_result['t3_mid_bcgs_ilu_nt_64'][option]],
              color='red',marker='x',ms=10,lw=1.8,
              linestyle='none',mew=2,label='BCGS-ILU-NT')

ax.set_ylim(1000,200000)

#ax.annotate('BCGS-ILU-NT',
#            (24, sim_result['t3_mid_bcgs_ilu_nt_64'][option]-50000.), 
#            fontsize=12, color='black')

scale_ideal = scale_dict[methods[0]][0] / \
              (np.array(scale_dict[methods[0]+'cores']))
ax.loglog(scale_dict[methods[0]+'cores'], scale_ideal, 'y--', label='Ideal NT')
#ax.loglog(scale_dict[methods[0]+'cores'], scale_ideal*0.80975, 
#          color='aqua',linestyle='--', label='Ideal NTR')
ax.tick_params(axis='x', labelsize=16)
ax.tick_params(axis='y', labelsize=16)
ax.minorticks_off()
ax.set_xticks(scale_dict[methods[0]+'cores'])
ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
ax.set_yticks([1000,10000,100000])
ax.set_title('Strong Scaling', fontsize=16)
ax.set_xlabel('Number of Cores', fontsize=16)
ax.set_ylabel('Wall-clock Time [sec]', fontsize=16)
#ax.annotate('4 cores per node (cpn)',
#            (3, sim_result[scale_no_packing[0]][option]-4200.), 
#            fontsize=12, color='black')
plt.legend(bbox_to_anchor=(-0.02, -0.2), loc='upper left', fontsize=14, ncol=2,
           frameon=False)
plt.savefig(save_img_path + '/' + 'Strong Scaling Mid Case', bbox_inches='tight', 
                    pad_inches=0.1, dpi=300)


#%%
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
label3=['Newton 2 cpn', 'Newton 8 cpn','Newton 16 cpn']
methods=['fca_nt_','fca_ntrdc_']
methods_name=['Newton','NTRDC']
# 1./(16/scale_dict[methods[i]][j])/(sim_result[scale_no_packing[j]][option]/scale_dict[cores_name][j])*100.
#eff2 = [85.3335, 72.0266, 74.3387]
# 1./(16/scale_dict['fca_nt_'][0])/(sim_result['t3_mid_fca_nt_02nodes']['Final_SNES_Time']/scale_dict['fca_nt_cores'][0])*100.
eff3_list=['t3_mid_fca_nt_16','t3_mid_fca_nt_02nodes',
           't3_mid_fca_nt_04nodes4','t3_mid_fca_nt_08nodes']
eff3=[]
for name in eff3_list:
    eff3.append(1./(16/scale_dict['fca_nt_'][0])/
                (sim_result[name]['Final_SNES_Time'])*100.)
ms3  = [8,     8,     8,     8,   ]

eff4_list=['t3_mid_fca_nt_32','t3_mid_fca_nt_04nodes8',
           't3_mid_fca_nt_08nodes4','t3_mid_fca_nt_16nodes2']
eff4=[]
for name in eff4_list:
    eff4.append(1./(32/scale_dict['fca_nt_'][0])/
                (sim_result[name]['Final_SNES_Time'])*100.)

eff5_list=['t3_mid_fca_nt_64','t3_mid_fca_nt_08nodes8',
           't3_mid_fca_nt_16nodes4','t3_mid_fca_nt_32nodes2']
eff5=[]
for name in eff5_list:
    eff5.append(1./(64/scale_dict['fca_nt_'][0])/
                (sim_result[name]['Final_SNES_Time'])*100.)

eff6_list=['t3_mid_fca_ntrdc_04nodes4','t3_mid_fca_ntrdc_08nodes4',
           't3_mid_fca_ntrdc_16nodes4']
core_list=[16,32,64]
eff6 = []
for i in range(len(eff6_list)):
    eff6.append(1./(core_list[i]/scale_dict['fca_ntrdc_'][0])/
                (sim_result[eff6_list[i]]['Final_SNES_Time'])*100.)

colors=['magenta','green','red','blue','green','cyan',
        'fuchsia','aqua']
lines=['-','-','-.','-.','-','-.','--','-']
markers=['','v','x','*','x','o','^']
markers2=['o','^','v','s','>']

for i in range(len(methods)):
    ax.semilogx(scale_dict[methods[i]+'cores'], scale_dict[methods[i]+'eff'],
                color=colors[i],marker=markers[i],lw=1.8,
                linestyle=lines[i],label=methods_name[i])
label3=['Newton 16 cpn','Newton 8 cpn','Newton 4 cpn','Newton 2 cpn']
label4=['NTR 4 cpn','NTRDC 4 cpn']
for i in [0,1,2,3]:
    ax.semilogx([16,32,64], [eff3[i],eff4[i],eff5[i]],
                color=colors[i],marker='o',lw=1.8,ms=ms3[i],mew=1.5,
                 linestyle='solid',fillstyle='none',label=label3[i])

ax.semilogx([16,32,64], eff6,
            color='red',marker=markers2[2],lw=1.8,ms=12,mew=1.5,
            linestyle='none',fillstyle='none',label=label4[1])

scale_ideal_eff = np.ones(len(scale_dict[methods[0]]))*100.
ax.semilogx(scale_dict[methods[0]+'cores'], scale_ideal_eff, 
            color='y', linestyle='--', label='Ideal')


ax.tick_params(axis='x', labelsize=16)
ax.tick_params(axis='y', labelsize=16)
ax.minorticks_off()
ax.set_xticks(scale_dict[methods[0]+'cores'])
ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
ax.set_title('Strong Scaling Efficiency', fontsize=16)
ax.set_xlabel('Number of Cores', fontsize=16)
ax.set_ylabel('Efficiency [%]', fontsize=16)
ax.set_ylim([0,110])
plt.legend(bbox_to_anchor=(-0.02, -0.2), loc='upper left', fontsize=14, ncol=2,
           frameon=False)
plt.savefig(save_img_path + '/' + 'Strong Scaling Efficiency', 
            bbox_inches='tight', pad_inches=0.1, dpi=300)
