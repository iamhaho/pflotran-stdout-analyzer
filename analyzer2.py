#!/bin/env python
"""
Program to analyze screen output of PFLOTRAN.

@author: Heeho Park <heepark@sandia.gov>
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

import analyzer_io as io
import analyzer_post_process as pp

path = '/home/heepark/models/uz/boca/no_lgr_trial1_rescue_completed'
ext = 'stdout'

file_list, experiment_list = io.scanfolder(path,ext)
fixed_timestep, simulation_time = io.scan_screen_output(file_list)
sim_result = io.read_screen_output(file_list, fixed_timestep, simulation_time)
sim_cut_result = io.read_cut_reasons(file_list)
print (sim_result.keys())
print (sim_result[list(sim_result.keys())[0]].keys())
print (sim_cut_result[list(sim_cut_result.keys())[0]].keys())

save_img_path = '/home/heepark/models/uz/image'

#%%
nl_solvers = ['v1_nrf_24pwr_ntrd','v1_nrf_24pwr_ntrdca','v1_nrf_24pwr_ntrdhi',
              'v2_nrf_24pwr_ntrdca','v1_nrf_24pwr_ntrdahi','v1_nrf_24pwr_ntrdcahi',
              'v2_nrf_24pwr_ntrda','v2_nrf_24pwr_ntrdcahi','v1_nrf_24pwr_ntrdc',
              'v1_nrf_24pwr_ntrdchi','v2_nrf_24pwr_ntrdahi','v2_nrf_24pwr_ntrdchi']

pp.plot_dual_ax_bar_graph(sim_result, option='Final_SNES_Time',
                          experiments=nl_solvers, timeunit='min',
                          save_img_path=save_img_path,
                          save_img_name='nl_solvers',
                          custom_xlabel=['BCGS-CPR-ABF',
                                         'FGMRES-CPR-ABF',
                                         'GMRES-CPR-ABF'],
                          legend_flag=False,
                          custom_title='Linear Solver Computation Time\n'+
                                       'Newton Easy Case (8-core)')

#%%
nl_solvers = ['v1_nrf_12pwr_nt',
              'v1_nrf_12pwr_ntrd',
              'v1_nrf_12pwr_ntrdahi',
              'v1_nrf_12pwr_ntrdc',
              'v1_nrf_12pwr_ntrdca',
              'v1_nrf_12pwr_ntrdcahi',
              'v1_nrf_12pwr_ntrdchi',
              'v1_nrf_12pwr_ntrdhi'
              ]

pp.plot_dual_ax_bar_graph(sim_result, option='Final_SNES_Time',
                          experiments=nl_solvers, timeunit='min',
                          save_img_path=save_img_path,
                          save_img_name='nl_solvers',
                          custom_xlabel=['BCGS-CPR-ABF',
                                         'FGMRES-CPR-ABF',
                                         'GMRES-CPR-ABF'],
                          legend_flag=False,
                          custom_title='Linear Solver Computation Time\n'+
                                       'Newton Easy Case (8-core)')

